#
# Sidebar Timer - Init script
#

#Import dependencies
function gametimer:init

#Objectives initialization
function timer_display:gameboard_name

#End of init
tellraw @a [{"text":"[timer_display] ","color":"gold"},{"text":"System initiated, ready to run.","color":"yellow"}]