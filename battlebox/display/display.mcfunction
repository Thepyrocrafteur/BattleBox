scoreboard players reset * GI

scoreboard objectives setdisplay list PK
scoreboard objectives setdisplay sidebar GI
scoreboard players set §5§lTeams: GI -3
scoreboard players set §b GI -5
scoreboard players set §9§lPlayers: GI -6
scoreboard players set §a GI -8
function battlebox:display/teams
function battlebox:display/players
function timer_display:display/sidebar_timer if @e[tag=SystemHandler,name=SystemHandler,score_IWR=0]
function timer_display:display/sidebar_alt_timer if @e[tag=SystemHandler,name=SystemHandler,score_IWR_min=1]