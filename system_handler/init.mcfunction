#
# System Handler - Init script
#

#Structure initialization
fill ~ ~ ~ ~8 ~8 ~8 bedrock 0
fill ~1 ~1 ~ ~7 ~7 ~8 barrier 0
fill ~ ~1 ~1 ~8 ~7 ~7 barrier 0
fill ~1 ~ ~1 ~7 ~8 ~7 barrier 0
fill ~1 ~1 ~1 ~7 ~7 ~7 stained_hardened_clay 9
fill ~2 ~2 ~2 ~6 ~6 ~6 air 0
setblock ~4 ~2 ~4 sea_lantern 0

#Kill all existing SystemHandler
kill @e[tag=SystemHandler,name=SystemHandler]

#Summon SystemHandler
summon armor_stand ~4 ~3 ~4 {CustomName:"SystemHandler",Tags:["SystemHandler","persistant"]}

#End of Init
tellraw @a [{"text":"[System_handler] ","color":"light_purple"},{"text":"Ready to order!","color":"light_purple"}]