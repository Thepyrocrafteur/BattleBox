worldborder add 600 30
gamemode 1 @a[m=!3]
scoreboard players reset * GI
scoreboard players set §a§l GI -1
scoreboard players set §d§lWinners: GI -2
scoreboard players set @a[m=1] GI -3
scoreboard players set §b§l GI -4
execute @a ~ ~ ~ title @s title {"text":"GAME OVER","color":"red"}
execute @a ~ ~ ~ title @s subtitle ["",{"text":"#","color":"gold"},{"text":"TEAM","color":"yellow"},{"text":"TOP","color":"gold"},{"score":{"name":"@s","objective":"TR"},"color":"yellow"},{"text":"  ","color":"gold"},{"text":"|","color":"dark_gray"},{"text":"  #","color":"gold"},{"text":"SOLO","color":"yellow"},{"text":"TOP","color":"gold"},{"score":{"name":"@s","objective":"PR"},"color":"yellow"}]
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GS -2