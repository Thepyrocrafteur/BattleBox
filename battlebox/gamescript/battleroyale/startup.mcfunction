scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GTM 5
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GTS 45

scoreboard players set @e[tag=SystemHandler,name=SystemHandler] WRL 7

scoreboard players reset @a

scoreboard players tag remove @a[tag=admin] admin

execute @e[tag=SystemHandler,name=SystemHandler] ~ ~ ~ worldborder center ~ ~
execute @e[tag=SystemHandler,name=SystemHandler] ~ ~ ~ worldborder set 2499 0
worldborder damage buffer 3
worldborder damage amount 0.05

effect @a clear

effect @a minecraft:slowness 15 255 true
effect @a minecraft:blindness 15 255 true
effect @a minecraft:weakness 15 255 true
effect @a minecraft:jump_boost 15 128 true
effect @a minecraft:resistance 30 255 true
effect @a minecraft:instant_health 10 10 true
effect @a minecraft:health_boost 7200 4 true

replaceitem entity @a[team=!Spectator] slot.hotbar.4 compass 1 0

title @a times 0 80 10

execute @e[tag=SystemHandler,name=SystemHandler] ~ ~ ~ spreadplayers ~ ~ 500 1200 true @a

scoreboard players set @a PR 1
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GS 2