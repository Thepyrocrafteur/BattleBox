#
# MGD_battleroyale - Init script
#

#Call dependencies before init
function timer_display:init

#Const initialization
scoreboard objectives add c05 dummy const05
scoreboard objectives add c10 dummy const10
scoreboard objectives add c20 dummy const20

#Objectives initialization
scoreboard objectives add GS dummy GameStatus
scoreboard objectives add WRL dummy WallReductionLeft
scoreboard objectives add WRO dummy WallReductionOccurred
scoreboard objectives add IWR dummy InWallReduction
scoreboard objectives add PL dummy PlayerLeft
scoreboard objectives add TL dummy TeamLeft
scoreboard objectives add PR dummy PlayerRank
scoreboard objectives add TR dummy TeamRank
scoreboard objectives add PK playerKillCount PlayerKill
scoreboard objectives add PIO deathCount PlayerIsOut
scoreboard objectives add TS dummy TeamSize
scoreboard objectives add PIT dummy PlayerInTeam
scoreboard objectives add GC dummy GarbageCollector
scoreboard objectives add ET dummy EventTimer
scoreboard objectives add tmp dummy temp

#Teams creation
scoreboard teams add Spectator
scoreboard teams add T1
scoreboard teams add T2
scoreboard teams add T3
scoreboard teams add T4
scoreboard teams add T5
scoreboard teams add T6
scoreboard teams add T7
scoreboard teams add T8
scoreboard teams add T9
scoreboard teams add T10
scoreboard teams add T11
scoreboard teams add T12
scoreboard teams add T13
scoreboard teams add T14
scoreboard teams add T15
scoreboard teams add T16
scoreboard teams add T17
scoreboard teams add T18
scoreboard teams add T19
scoreboard teams add T20
scoreboard teams add T21
scoreboard teams add T22
scoreboard teams add T23
scoreboard teams add T24
scoreboard teams add T25
scoreboard teams add T26
scoreboard teams add T27
scoreboard teams add T28

#Teams initialization
scoreboard teams option T1 color dark_blue
scoreboard teams option T2 color dark_green
scoreboard teams option T3 color dark_aqua
scoreboard teams option T4 color dark_red
scoreboard teams option T5 color dark_purple
scoreboard teams option T6 color gold
scoreboard teams option T7 color gray
scoreboard teams option T8 color dark_gray
scoreboard teams option T9 color blue
scoreboard teams option T10 color green
scoreboard teams option T11 color aqua
scoreboard teams option T12 color red
scoreboard teams option T13 color light_purple
scoreboard teams option T14 color yellow
scoreboard teams option T15 color dark_blue
scoreboard teams option T16 color dark_green
scoreboard teams option T17 color dark_aqua
scoreboard teams option T18 color dark_red
scoreboard teams option T19 color dark_purple
scoreboard teams option T20 color gold
scoreboard teams option T21 color gray
scoreboard teams option T22 color dark_gray
scoreboard teams option T23 color blue
scoreboard teams option T24 color green
scoreboard teams option T25 color aqua
scoreboard teams option T26 color red
scoreboard teams option T27 color light_purple
scoreboard teams option T28 color yellow

#Objects initialization
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] c05 5
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] c10 10
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] c20 20
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GS 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] IWR 0

#Worldborder initialization
execute @e[tag=SystemHandler,name=SystemHandler] ~ ~ ~ worldborder center ~ ~
worldborder set 149 0

#Loop Function initialization (physical init)
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-1 ~-1 ~-2 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function battlebox:gamescript/battleroyale/startup if @e[tag=SystemHandler,name=SystemHandler,score_GS_min=1,score_GS=1]"}
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-2 ~-1 ~-2 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function battlebox:gamescript/battleroyale/executor if @e[tag=SystemHandler,name=SystemHandler,score_GS_min=2,score_GS=2]"}
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-2 ~-1 ~-1 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function battlebox:admin/executor if @e[tag=SystemHandler,name=SystemHandler,score_GS_min=0,score_GS=0]"}
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-2 ~-1 ~0 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function battlebox:game_event/win if @e[tag=SystemHandler,name=SystemHandler,score_GS_min=-1,score_GS=-1]"}
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-2 ~-1 ~1 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function battlebox:security"}
execute @e[name=SystemHandler,tag=SystemHandler] ~ ~ ~ setblock ~-2 ~-1 ~2 minecraft:repeating_command_block 0 replace {auto:1b,Command:"function timer_display:main if @e[tag=SystemHandler,name=SystemHandler,score_GS_min=2,score_GS=2]"}

#End of Init
tellraw @a [{"text":"[BattleBox] ","color":"light_purple"},{"text":"BattleBox initiated. Ready for action!","color":"dark_purple"}]
