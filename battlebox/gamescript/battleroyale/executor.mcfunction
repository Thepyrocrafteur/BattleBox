function battlebox:gamescript/battleroyale/event/airdropexec
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=39,score_GTS=44,score_GTT_min=-19,score_GTT=-19] ~ ~ ~ execute @a[team=!Spectator] ~ ~ ~ function battlebox:gamescript/battleroyale/event/airdrop

execute @e[tag=SystemHandler,name=SystemHandler,score_GTS_min=59,score_GTS=59,score_GTT_min=-19,score_GTT=-19] ~ ~ ~ execute @r[team=!Spectator] ~ ~ ~ function battlebox:gamescript/battleroyale/event/airdrop

execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=35,score_GTS=35] ~ ~ ~ title @a title {"text":" "}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=35,score_GTS=35] ~ ~ ~ title @a subtitle {"text":"5","color":"gold"}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=34,score_GTS=34] ~ ~ ~ title @a title {"text":" "}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=34,score_GTS=34] ~ ~ ~ title @a subtitle {"text":"4","color":"gold"}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=33,score_GTS=33] ~ ~ ~ title @a title {"text":"3","color":"red"}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=33,score_GTS=33] ~ ~ ~ title @a subtitle {"text":" "}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=32,score_GTS=32] ~ ~ ~ title @a title {"text":"2","color":"red"}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=31,score_GTS=31] ~ ~ ~ title @a title {"text":"1","color":"red"}
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=5,score_GTM=5,score_GTS_min=30,score_GTS=30] ~ ~ ~ title @a title {"text":"Fight !","color":"light_purple"}

execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=7,score_WRL=7,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_7
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=6,score_WRL=6,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_6
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=5,score_WRL=5,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_5
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=4,score_WRL=4,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_4
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=3,score_WRL=3,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_3
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=2,score_WRL=2,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_2
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=1,score_WRL=1,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_1
execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_WRL_min=0,score_WRL=0,score_IWR=0] ~ ~ ~ function battlebox:gamescript/battleroyale/reductions/reduction_0

execute @e[tag=SystemHandler,name=SystemHandler,score_IWR=0] ~ ~ ~ function timer_display:display/actionbar_timer
execute @e[tag=SystemHandler,name=SystemHandler,score_IWR_min=1] ~ ~ ~ function timer_display:display/actionbar_alt_timer

execute @e[tag=SystemHandler,name=SystemHandler,score_GTM_min=0,score_GTM=0,score_GTS_min=0,score_GTS=0,score_IWR_min=1] ~ ~ ~ function battlebox:gamescript/battleroyale/in_reduction

function battlebox:game_event/out
execute @e[tag=SystemHandler,name=SystemHandler,score_GTT_min=19,score_GTT=19] ~ ~ ~ function battlebox:display/display

scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_TL_min=1,score_TL=1,score_GS_min=0] GS -1
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_PL_min=1,score_PL=1,score_GS_min=0] GS -1