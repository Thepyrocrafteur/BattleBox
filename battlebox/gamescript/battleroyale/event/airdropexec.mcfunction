execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace leaves
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace leaves2
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace spruce_fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace nether_brick_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace birch_fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace jungle_fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace dark_oak_fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace acacia_fence_gate
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace spruce_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace birch_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace jungle_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace dark_oak_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace acacia_fence
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace cobblestone_wall
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace snow_layer
execute @e[tag=Crate] ~ ~ ~ fill ~ ~ ~ ~ ~-3 ~ air 0 replace ice
execute @e[tag=Crate] ~ ~ ~ playsound minecraft:entity.firework.launch ambient @a ~ ~ ~ 7 2
execute @e[tag=Crate] ~ ~ ~ particle smoke ~ ~ ~ 0 0 0 0.5 20 force @a[r=250]
execute @e[tag=CrateOnGround] ~ ~ ~ particle smoke ~ ~ ~ 0 5 0 0 20 force @a[r=250]
execute @e[tag=CrateInAir,type=armor_stand] ~ ~ ~ detect ~ ~ ~ dropper 0 scoreboard players tag @s add TmpCrateOnGround
execute @e[tag=CrateInAir,type=armor_stand] ~ ~ ~ detect ~ ~-1 ~ dropper 0 scoreboard players tag @s add TmpCrateOnGround
execute @e[tag=TmpCrateOnGround,type=armor_stand] ~ ~ ~ playsound minecraft:entity.firework.large_blast_far ambient @a ~ ~ ~ 10 0.5
execute @e[tag=TmpCrateOnGround] ~ ~ ~ scoreboard players tag @s remove CrateInAir
execute @e[tag=TmpCrateOnGround] ~ ~ ~ scoreboard players tag @s add CrateOnGround
execute @e[tag=TmpCrateOnGround] ~ ~ ~ scoreboard players tag @s remove TmpCrateOnGround
scoreboard players add @e[tag=CrateOnGround] ET 1
kill @e[tag=CrateOnGround,score_ET_min=1200]