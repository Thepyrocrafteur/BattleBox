scoreboard players set @e[tag=SystemHandler,name=SystemHandler] IWR 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] WRO 1

scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=6,score_WRL=6,score_IWR=0] GTS 4
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=5,score_WRL=5,score_IWR=0] GTS 30
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=4,score_WRL=4,score_IWR=0] GTS 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=3,score_WRL=3,score_IWR=0] GTS 30
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=2,score_WRL=2,score_IWR=0] GTS 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=1,score_WRL=1,score_IWR=0] GTS 30
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=0,score_WRL=0,score_IWR=0] GTS 0

scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=6,score_WRL=6,score_IWR=0] GTM 4
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=5,score_WRL=5,score_IWR=0] GTM 3
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=4,score_WRL=4,score_IWR=0] GTM 3
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=3,score_WRL=3,score_IWR=0] GTM 2
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=2,score_WRL=2,score_IWR=0] GTM 2
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=1,score_WRL=1,score_IWR=0] GTM 1
scoreboard players set @e[tag=SystemHandler,name=SystemHandler,score_WRL_min=0,score_WRL=0,score_IWR=0] GTM 3