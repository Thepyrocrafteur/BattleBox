#
# GameTimer - Init Script
#

#Dependencies
function system_handler:init

#Objectives initialization
scoreboard objectives add GTT dummy GameTimerTick
scoreboard objectives add GTS dummy GameTimeSeconds
scoreboard objectives add GTM dummy GameTimerMinutes

scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GTT 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GTS 0
scoreboard players set @e[tag=SystemHandler,name=SystemHandler] GTM 0

#End of Init
tellraw @a [{"text":"[Game_timer] ","color":"light_purple"},{"text":"Ready to order!","color":"light_purple"}]